# Odjazdy MPK Poznań #

This is a mobile app built with [Ionic Framework v1](https://ionicframework.com/). App shows buses and trams departures from stops in real time. App make use of *[Wirtualny Monitor - PEKA](https://www.peka.poznan.pl/vm/)* and *[Poznań Api](http://www.poznan.pl/api/)*.

## Links ##
### Android ###
* [Google Play](https://play.google.com/store/apps/details?id=com.ionicframework.virtualmonitor917929)

## Run Locally ##
This assumes that you've already had an emulator setup for iOS or Android. If you haven't had yet, pleace look guide [android](http://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html), [os x](http://cordova.apache.org/docs/en/latest/guide/platforms/osx/index.html).


```
npm install -g cordova
git clone https://bitbucket.org/mariusz_bednarek/virtualmonitor
cd virtualmonitor
npm install
bower install
ionic platform add android
ionic emulate android

```
If you have a problem, look guide on [Ionic website](https://ionicframework.com/docs/guide/installation.html).

## Testing ##


```

gulp test

```


## Platforms ##
App is tested and published on Android platform. However Ionic Framework allows building and publishing app on other mobile platforms.


## LICENSE ##

The project is licensed under the GNU General Public License version 3 (or newer).