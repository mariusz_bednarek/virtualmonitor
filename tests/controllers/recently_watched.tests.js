describe('RecentlyWatchedCtrl', function() {
  beforeEach(module('app'));

  var $controller, StopFactory, $rootScope, stops;
  //
  beforeEach(inject(function (_$controller_, _StopFactory_, _$rootScope_, _$localStorage_) {
    $controller = _$controller_;
    StopFactory = _StopFactory_;
    $rootScope = _$rootScope_;
    $localStorage = _$localStorage_;
    stops = {
      one: {bollard: {name: 'One', tag: 'one'}, directions: [{lineName: 1, direction: 'Direction 1'}]},
      two: {bollard: {name: 'Two', tag: 'two'}, directions: [{lineName: 3, direction: 'Direction aaa'}]}
    };
    }));

  describe('$scope.stops', function() {
    var $scope, stateParams, controller;
    beforeEach(function() {
      $localStorage.$reset();
      $scope = $rootScope.$new();
    });
    it('recently watched stops length should be two', function() {
      StopFactory.addToRecently(stops.one);
      StopFactory.addToRecently(stops.one);
      StopFactory.addToRecently(stops.two);
      StopFactory.addToRecently(stops.two);
      controller = $controller('RecentlyWatchedCtrl', { $scope: $scope});
      expect($scope.stops.length).toEqual(2);
    });
    it('recently watched stops should contains two', function() {
      StopFactory.addToRecently(stops.one);
      StopFactory.addToRecently(stops.two);
      controller = $controller('RecentlyWatchedCtrl', { $scope: $scope});
      expect($scope.stops).toContain(stops.two);
    });
    it("stop two's directions should be change", function() {
      var stop_two = angular.copy(stops.two);
      stop_two['directions'][0].lineName = 2;
      StopFactory.addToRecently(stops.two);
      StopFactory.addToRecently(stop_two);
      controller = $controller('RecentlyWatchedCtrl', { $scope: $scope});
      expect($scope.stops[0]['directions']).toContain({lineName: 2, direction: 'Direction aaa'});
    });

  });


});
