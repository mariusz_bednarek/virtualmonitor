describe('PointStopsCtrl', function() {
  beforeEach(module('app'));

  var $controller, VirtualMonitor, $rootScope, $httpBackend, pekaApi,
    serverResponse = {success:{bollards: [
      {bollard: {name: 'ABC', tag: 'abc'}, directions: [{lineName: '1', direction: 'Hell'}]},
      {bollard: {name: 'DEF', tag: 'def'}, directions: [{lineName: '2', direction: 'Dir 1'}, {lineName: '2', direction: 'Dir 2'}]}
    ]}};
  //
  beforeEach(inject(function (_$controller_, _$rootScope_, _VirtualMonitor_, _$httpBackend_, _pekaApi_) {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    VirtualMonitor = _VirtualMonitor_;
    $httpBackend = _$httpBackend_;
    pekaApi = _pekaApi_;
    $httpBackend.whenGET(/templates.*/).respond(200, '');
    }));

  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
     $httpBackend.verifyNoOutstandingRequest();
   });

  describe('$scope values', function() {
    var $scope, controller, stateParams;
    beforeEach(function() {
      $scope = $rootScope.$new();
      stateParams = {name: 'abc'};
      controller = $controller('PointStopsCtrl', { $scope: $scope, $stateParams: stateParams });
      $httpBackend.expectPOST(pekaApi).respond(serverResponse);
    });
    it('should get stops from server', function() {
      expect($scope.stops).toBeUndefined();
      $httpBackend.flush();
      expect($scope.stops).toBeDefined();
      expect($scope.stops).toContain({bollard: {name: 'ABC', tag: 'abc'}, directions: [{lineName: '1', direction: 'Hell'}]});
    });
    it('scope.point should be abc', function() {
      $httpBackend.flush();
      expect($scope.point).toEqual('abc');
    });
  });

});
