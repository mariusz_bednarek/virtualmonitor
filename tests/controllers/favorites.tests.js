describe('FavoritesCtrl', function() {
  beforeEach(module('app'));

  var $controller, StopFactory, $rootScope, stops;
  //
  beforeEach(inject(function (_$controller_, _StopFactory_, _$rootScope_, _$localStorage_) {
    $controller = _$controller_;
    StopFactory = _StopFactory_;
    $rootScope = _$rootScope_;
    $localStorage = _$localStorage_;
    stops = {
      one: {bollard: {name: 'One', tag: 'one'}, directions: [{lineName: 1, direction: 'Direction 1'}]},
      two: {bollard: {name: 'Two', tag: 'two'}, directions: [{lineName: 3, direction: 'Direction aaa'}]}
    };
    }));

  describe('$scope.stops', function() {
    var $scope, stateParams, controller;
    beforeEach(function() {
      $localStorage.$reset();
      $scope = $rootScope.$new();
    });
    it('favorites stops length should be two', function() {
      StopFactory.setFavorite(stops.one);
      StopFactory.setFavorite(stops.two);
      controller = $controller('FavoritesCtrl', { $scope: $scope});
      expect($scope.stops.length).toEqual(2);
    });
    it('recently watched stops should contains two', function() {
      StopFactory.setFavorite(stops.one);
      StopFactory.setFavorite(stops.two);
      controller = $controller('FavoritesCtrl', { $scope: $scope});
      expect($scope.stops).toContain(stops.two);
    });
  });
});
