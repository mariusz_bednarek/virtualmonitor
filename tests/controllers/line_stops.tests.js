describe('LineStopsCtrl', function() {
  beforeEach(module('app'));

  var $controller, VirtualMonitor, $rootScope, $httpBackend, pekaApi,
    serverResponse = {success:{directions: [
      {direction: {lineName: '13', direction: 'Junikowo'}, bollards: [{name: 'ABC', tag: 'abc'}, {name: 'DEF', tag: 'def'}]}
    ]}};
  //
  beforeEach(inject(function (_$controller_, _$rootScope_, _VirtualMonitor_, _$httpBackend_, _pekaApi_) {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    VirtualMonitor = _VirtualMonitor_;
    $httpBackend = _$httpBackend_;
    pekaApi = _pekaApi_;
    $httpBackend.whenGET(/templates.*/).respond(200, '');
    }));

  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
     $httpBackend.verifyNoOutstandingRequest();
   });

  describe('$scope values', function() {
    var $scope, controller, stateParams;
    beforeEach(function() {
      $scope = $rootScope.$new();
      stateParams = {name: '13'};
      controller = $controller('LineStopsCtrl', { $scope: $scope, $stateParams: stateParams });
      $httpBackend.expectPOST(pekaApi).respond(serverResponse);
    });
    it('should get directions from server', function() {
      expect($scope.directions).toBeUndefined();
      $httpBackend.flush();
      expect($scope.directions).toBeDefined();
      expect($scope.directions).toContain({direction: {lineName: '13', direction: 'Junikowo'}, bollards: [{name: 'ABC', tag: 'abc'}, {name: 'DEF', tag: 'def'}], visible: true});
    });
    it('direction.visible should be true', function() {
      $httpBackend.flush();
      expect($scope.directions[0].visible).toEqual(true);
    });
    it('direction.visible should be false', function() {
      $httpBackend.flush();
      $scope.toggleDirection($scope.directions[0]);
      expect($scope.directions[0].visible).toEqual(false);
    });
    it('scope.line should be 13', function() {
      $httpBackend.flush();
      expect($scope.line).toEqual('13');
    });
  });

});
