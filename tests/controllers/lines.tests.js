describe('LinesCtrl', function() {
  beforeEach(module('app'));

  var $controller, VirtualMonitor, $rootScope, $httpBackend, pekaApi,
    serverResponse = {success:[{name: '1'}, {name: '2'}, {name: '3'}]};
  //
  beforeEach(inject(function (_$controller_, _$rootScope_, _VirtualMonitor_, _$httpBackend_, _pekaApi_) {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    VirtualMonitor = _VirtualMonitor_;
    $httpBackend = _$httpBackend_;
    pekaApi = _pekaApi_;
    $httpBackend.whenGET(/templates.*/).respond(200, '');
    }));

  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
     $httpBackend.verifyNoOutstandingRequest();
   });

  describe('$scope values', function() {
    var $scope, controller;
    beforeEach(function() {
      $scope = $rootScope.$new();
      controller = $controller('LinesCtrl', { $scope: $scope });
      $httpBackend.expectPOST(pekaApi).respond(serverResponse);
    });
    it('should get lines from server', function() {
      expect($scope.lines).toBeUndefined();
      $httpBackend.flush();
      expect($scope.lines).not.toBeUndefined();
      expect($scope.lines).toContain({name: '2'});
      expect($scope.lines).not.toContain({name: '55'});
    });
    it('state should be: lines ', function() {
      $httpBackend.flush();
      expect($scope.state).toEqual('lines');
    });
    it('scope.search should initialized ', function() {
      $httpBackend.flush();
      expect($scope.search).toEqual({data: ''});
    });
  });

});
