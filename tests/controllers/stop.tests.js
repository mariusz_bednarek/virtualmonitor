describe('StopCtrl', function() {
  beforeEach(module('app'));

  var $controller, StopFactory, $rootScope, stops;
  //
  beforeEach(inject(function (_$controller_, _StopFactory_, _$rootScope_, _$localStorage_) {
    $controller = _$controller_;
    StopFactory = _StopFactory_;
    $rootScope = _$rootScope_;
    $localStorage = _$localStorage_;
    stops = {
      one: {name: 'One', tag: 'one'},
      two: {name: 'Two', tag: 'two'},
      three: {name: 'Three', tag: 'three'}
    };
    }));

  describe('$scope.stop', function() {
    var $scope, stateParams, controller;
    beforeEach(function() {
      $localStorage.$reset();
      $scope = $rootScope.$new();
      stateParams = stops['one'];
      controller = $controller('StopCtrl', { $scope: $scope, $stateParams: stateParams });
    });
    it('stop name should be One', function() {
      expect($scope.stop.bollard.name).toEqual('One');
    });
    it('stop tag should be one', function() {
      expect($scope.stop.bollard.tag).toEqual('one');
    });
    it('stop favorite should be false', function() {
      expect($scope.stop.favorite).toEqual(false);
    });
    it('stop favorite should be true', function() {
      StopFactory.setFavorite({bollard: stops['one']});
      $controller('StopCtrl', { $scope: $scope, $stateParams: stateParams });
      expect($scope.stop.favorite).toEqual(true);
    });
  });

  describe('favorite stops', function() {
    var $scope, stateParams, controller;
    beforeEach(function() {
      $localStorage.$reset();
      $scope = $rootScope.$new();
      stateParams = stops['three'];
      controller = $controller('StopCtrl', { $scope: $scope, $stateParams: stateParams });
    });

    it('stop should not be favorite', function() {
      $scope.stop.favorite = false;
      $scope.setFavorite();
      expect(StopFactory.isFavorite($scope.stop.bollard.tag)).toEqual(true);
    });

    it('stop should not be favorite', function() {
      $scope.stop.favorite = true;
      $scope.setFavorite();
      expect(StopFactory.isFavorite($scope.stop.bollard.tag)).toEqual(false);
    });
  });


});
