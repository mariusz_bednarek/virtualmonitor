describe('PointsCtrl', function() {
  beforeEach(module('app'));

  var $controller, VirtualMonitor, $rootScope, $httpBackend, pekaApi,
    serverResponse = {success:[{symbol: 'abc', name: 'ABC'}]};
  //
  beforeEach(inject(function (_$controller_, _$rootScope_, _VirtualMonitor_, _$httpBackend_, _pekaApi_) {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    VirtualMonitor = _VirtualMonitor_;
    $httpBackend = _$httpBackend_;
    pekaApi = _pekaApi_;
    $httpBackend.whenGET(/templates.*/).respond(200, '');
    }));

  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
     $httpBackend.verifyNoOutstandingRequest();
   });

  describe('$scope values', function() {
    var $scope, controller;
    beforeEach(function() {
      $scope = $rootScope.$new();
      controller = $controller('PointsCtrl', { $scope: $scope });
      $httpBackend.expectPOST(pekaApi).respond(serverResponse);
    });
    it('should get points from server', function() {
      expect($scope.points).toBeUndefined();
      $httpBackend.flush();
      expect($scope.points).not.toBeUndefined();
      expect($scope.points).toContain({symbol: 'abc', name: 'ABC'});
      expect($scope.points).not.toContain({symbol: 'def', name: 'ABC'});
    });
    it('state should be: points ', function() {
      $httpBackend.flush();
      expect($scope.state).toEqual('points');
    });
    it('scope.search should initialized ', function() {
      $httpBackend.flush();
      expect($scope.search).toEqual({data: ''});
    });
  });

});
