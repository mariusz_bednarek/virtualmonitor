describe('StreetsCtrl', function() {
  beforeEach(module('app'));

  var $controller, VirtualMonitor, $rootScope, $httpBackend, pekaApi,
    serverResponse = {success:[{name: 'Street 1'}, {name: 'Street 9'}]};
  //
  beforeEach(inject(function (_$controller_, _$rootScope_, _VirtualMonitor_, _$httpBackend_, _pekaApi_) {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    VirtualMonitor = _VirtualMonitor_;
    $httpBackend = _$httpBackend_;
    pekaApi = _pekaApi_;
    $httpBackend.whenGET(/templates.*/).respond(200, '');
    }));

  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
     $httpBackend.verifyNoOutstandingRequest();
   });

  describe('$scope values', function() {
    var $scope, controller;
    beforeEach(function() {
      $scope = $rootScope.$new();
      controller = $controller('StreetsCtrl', { $scope: $scope });
      $httpBackend.expectPOST(pekaApi).respond(serverResponse);
    });
    it('should get streets from server', function() {
      expect($scope.streets).toBeUndefined();
      $httpBackend.flush();
      expect($scope.streets).not.toBeUndefined();
      expect($scope.streets).toContain({name: 'Street 9'});
      expect($scope.streets).not.toContain({name: 'Street 8'});
    });
    it('state should be: streets ', function() {
      $httpBackend.flush();
      expect($scope.state).toEqual('streets');
    });
    it('scope.search should initialized ', function() {
      $httpBackend.flush();
      expect($scope.search).toEqual({data: ''});
    });
  });

});
