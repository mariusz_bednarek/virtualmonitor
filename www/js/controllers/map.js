angular.module('app')
.controller('MapCtrl', function($scope, PoznanApi, $state, MapApi, $ionicLoading, Popups) {
  $scope.map = { center: { latitude: 52.4064, longitude: 16.9252 }, zoom: 11, control: {} };
  var marker = null;

  $scope.$on('$destroy', function() {
    MapApi.clearMap();
  });

  $ionicLoading.show();

  MapApi.init()
  .then(function(_api) {
    initMarkers();
    getCurrentPosition();
  });

  function getCurrentPositionSuccess(position) {
    var markerData;
    markerData = {
      position: {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      },
      icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
      info: 'Twoja pozycja'
    };
    marker = MapApi.initMarker(markerData, true);
    marker.addListener('click', function(e,b) {
      showInfoWindow(this)
    });
    MapApi.setPosition(position);
  }

  function getCurrentPositionError() {
    Popups.getCurrentPositionError()
    .then(function(res) {
      if (res) {
        getCurrentPosition();
      }
    });
  }

  function initMarkers() {
    var markers = [],
      markerData;
    PoznanApi.getStops()
    .then(function(stops) {
      stops.forEach(function(stop) {
        markerData = {
          position: {
            lat: stop.geometry.coordinates[1],
            lng: stop.geometry.coordinates[0]
          },
          stopId: stop.id,
          info: stopinfo(stop)
        };
        marker = MapApi.initMarker(markerData);
        markers.push(marker);
        marker.addListener('click', function(e,b) {
          showInfoWindow(this)
        });
      });
      MapApi.markerClusterer(markers);
      $ionicLoading.hide();
    });
  }

  function showInfoWindow(_marker) {
    MapApi.showInfoWindow(_marker);
  }

  function stopinfo(stop) {
    return "<a href='" +
      $state.href('app.stop', {tag: stop.id, name: stop.properties.stop_name}) +
      "' style='color: inherit; text-decoration: none;'><strong>" +
      stop.properties.stop_name +
      "</strong><br/>" +
      stop.properties.headsigns +
      "</a>";
  }

  function getCurrentPosition() {
    navigator.geolocation.getCurrentPosition(
      getCurrentPositionSuccess,
      getCurrentPositionError,
      {timeout: 3000 }
    );
  }


});
