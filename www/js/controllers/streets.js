angular.module('app')
.controller('StreetsCtrl', function($scope, VirtualMonitor) {
  $scope.search = {data: ''};
  $scope.state = 'streets';
  getStreets();

  $scope.getStreets = function() {
    getStreets();
  };

  function getStreets() {
    VirtualMonitor.getStreets($scope.search.data)
    .then(function(res){
      $scope.streets = res;
    });
  }
});
