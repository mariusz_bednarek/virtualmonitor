angular.module('app')
.controller('LineStopsCtrl', function($scope, VirtualMonitor, $stateParams) {
  $scope.line = $stateParams.name;

  $scope.toggleDirection = function(direction) {
    direction.visible = !direction.visible;
  };

  VirtualMonitor.getBollardsByLine($scope.line)
  .then(function(res){
    $scope.directions = res.directions;
    if ($scope.directions.length == 1) {
      $scope.directions[0].visible = true;
    }
  });
});
