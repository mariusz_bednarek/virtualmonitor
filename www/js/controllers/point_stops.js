angular.module('app')
.controller('PointStopsCtrl', function($scope, VirtualMonitor, $stateParams) {
  $scope.point = $stateParams.name;

  VirtualMonitor.getBollardsByStopPoint($scope.point)
  .then(function(res) {
    $scope.stops = res.bollards;
  });
});
