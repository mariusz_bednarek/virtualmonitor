angular.module('app')
.controller('FavoritesCtrl', function($scope, StopFactory) {
  $scope.stops = StopFactory.getFavorites();
});
