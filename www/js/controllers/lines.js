angular.module('app')
.controller('LinesCtrl', function($scope, VirtualMonitor) {
  $scope.search = {data: ''};
  $scope.state = 'lines';
  getLines();

  $scope.getLines = function() {
    getLines();
  };

  function getLines() {
    VirtualMonitor.getLines($scope.search.data)
    .then(function(res){
      $scope.lines = res;
    });
  }
});
