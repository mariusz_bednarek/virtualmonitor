angular.module('app')
.controller('TabsCtrl', function($scope, $ionicHistory) {
  $scope.onTabDeselected = function() {
    $ionicHistory.clearHistory();
  };
});
