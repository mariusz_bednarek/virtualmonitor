angular.module('app')
.controller('PointsCtrl', function($scope, VirtualMonitor) {
  $scope.search = {data: ''};
  $scope.state = 'points';
  getPoints();

  $scope.getPoints = function() {
    getPoints();
  };

  function getPoints() {
    VirtualMonitor.getStopPoints($scope.search.data)
    .then(function(res){
      $scope.points = res;
    });
  }
});
