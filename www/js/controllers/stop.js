angular.module('app')
.controller('StopCtrl', function($scope, VirtualMonitor, $stateParams, $interval, StopFactory, PoznanApi, $timeout, $ionicPopup) {
  $scope.stop = {
    bollard: {
      name: $stateParams.name,
      tag: $stateParams.tag,
    },
    favorite: StopFactory.isFavorite($stateParams.tag),
    directions: []
  };
  var timeout = null;
  getTimes();
  getMessage();

  $scope.$on('device_resume', getTimes);
  $scope.$on('device_pause', stopTimeout);
  $scope.$on('$destroy', stopTimeout);

  PoznanApi.stopDirections($scope.stop.bollard.tag)
  .then(function(res) {
    if (res.length > 0) {
      $scope.stop.directions = res;
      StopFactory.addToRecently(angular.copy($scope.stop));
      if ($scope.stop.favorite) {
        StopFactory.updateFavorite($scope.stop);
      }
    }
  });


  $scope.setFavorite = function() {
    $scope.stop.favorite = !$scope.stop.favorite;
    if ($scope.stop.favorite) {
      StopFactory.setFavorite(angular.copy($scope.stop));
    } else {
      StopFactory.removeFavorite($scope.stop.bollard.tag);
    }
  };

  $scope.messageAlert = function(message) {
    $ionicPopup.alert({
     title: 'Informacja',
     template: message
   });
  };

  $scope.showHelpMessage = function() {
    $ionicPopup.alert({
     title: 'Pomoc',
     template: 'Tabela prezentuje rzeczywisty czas odjazdu autobusu/tramwaju z danego przystanku. Jeżeli dla danej lini nie ma podanego czasu odjazdu w minutach (zaznaczony na zielono), oznacza to, że pojazd jeszcze nie rozpoczął kursu, lub nastąpiła awaria i pojazd mimo że znajduje się na trasie to nie wysyła informacji o swoim położeniu.'
   });
  };

  function stopTimeout() {
    if (timeout) {
      $timeout.cancel(timeout);
      timeout = null;
    }
  }

  function reloadTimes() {
    timeout = $timeout(getTimes, 20000);
  }

  function getTimes() {
    $scope.timesDate = new Date();
    VirtualMonitor.getTimes($scope.stop.bollard.tag)
    .then(function(res) {
      $scope.times = res.times;
      reloadTimes();
    });
  }

  function getMessage() {
    VirtualMonitor.findMessagesForBollard($scope.stop.bollard.tag)
    .then(function(messages) {
      if (messages.length > 0) {
        $scope.message = messages.map(function(m) { return m.content; }).join(', ');
      }
    });
  }
});
