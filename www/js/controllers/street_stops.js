angular.module('app')
.controller('StreetStopsCtrl', function($scope, VirtualMonitor, $stateParams) {
  $scope.street = $stateParams.name;

  VirtualMonitor.getBollardsByStreet($scope.street)
  .then(function(res){
    $scope.stops = res.bollards;
  });
});
