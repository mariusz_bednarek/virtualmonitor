angular.module('app')
.directive('stopsList', function() {
  return {
    restrict: 'E',
    scope: {
      stops: '=',
      state: '@',
    },
    templateUrl: 'templates/_stops_list.html'
  };
});
