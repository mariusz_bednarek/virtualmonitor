// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'ngResource', 'ngStorage', 'uiGmapgoogle-maps', 'templates'])
.constant('pekaApi', 'http://odjazdy-mpk.m-bednarek.ovh')
.constant('poznanApiStopInfo', 'http://www.poznan.pl/mim/komunikacja/service.html')
.constant('poznanApiStops', 'http://www.poznan.pl/mim/plan/map_service.html?mtype=pub_transport&co=cluster')
.constant('$ionicLoadingConfig', {
  template: 'Ładowanie...',
  delay: 200,
  duration: 10000
})
.run(function($ionicPlatform, Popups, $document, $state, $rootScope, PoznanApi) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    if (window.StatusBar) {
      StatusBar.styleDefault();
    }

    document.addEventListener("offline", function() {
      Popups.noInternetConnection();
    }, false);

    document.addEventListener("resume", function() {
      Popups.enablePopups();
      $rootScope.$broadcast('device_resume');
    }, false);

    document.addEventListener("pause", function() {
      Popups.disablePopups();
      $rootScope.$broadcast('device_pause');
    }, false);

     document.addEventListener("deviceready", function() {
       PoznanApi.getStops();
     }, false);

  });
})

.config(function($ionicConfigProvider, $logProvider, $compileProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.tabs.style('standard');
  $ionicConfigProvider.navBar.alignTitle('center');
  $ionicConfigProvider.views.transition('none');
  $ionicConfigProvider.scrolling.jsScrolling(false);
  $logProvider.debugEnabled(false);
  $compileProvider.debugInfoEnabled(false);
})
.config(function(uiGmapGoogleMapApiProvider) {
  uiGmapGoogleMapApiProvider.configure({
    key: 'AIzaSyDmBm7KL1_9oFdggudNA5KG-MC7L0w7KUA',
    v: '3.25'
  });
})
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('app', {
    abstract: true,
    templateUrl: 'templates/menu.html'
  })
  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html'
      }
    }
  })
  .state('app.points', {
    url: '/points',
    views: {
      'menuContent': {
        templateUrl: 'templates/points.html',
        controller: 'PointsCtrl'
      }
    }
  })
  .state('app.point_stops', {
    url: '/point_stops/:name',
    views: {
      'menuContent': {
        templateUrl: 'templates/point_stops.html',
        controller: 'PointStopsCtrl'
      }
    }
  })
  .state('app.stop', {
    url: '/stop/:tag/:name',
    views: {
      'menuContent': {
        templateUrl: 'templates/stop.html',
        controller: 'StopCtrl'
      }
    }
  })
  .state('app.streets', {
    url: '/streets',
    views: {
      'menuContent': {
        templateUrl: 'templates/streets.html',
        controller: 'StreetsCtrl'
      }
    }
  })
  .state('app.street_stops', {
    url: '/street_stpps/:name',
    views: {
      'menuContent': {
        templateUrl: 'templates/street_stops.html',
        controller: 'StreetStopsCtrl'
      }
    }
  })
  .state('app.lines', {
    url: '/lines',
    views: {
      'menuContent': {
        templateUrl: 'templates/lines.html',
        controller: 'LinesCtrl'
      }
    }
  })
  .state('app.line_stops', {
    url: '/line_stops/:name',
    views: {
      'menuContent': {
        templateUrl: 'templates/line_stops.html',
        controller: 'LineStopsCtrl'
      }
    }
  })
  .state('app.favorites', {
    url: '/favorites',
    views: {
      'menuContent': {
        templateUrl: 'templates/favorites.html',
        controller: 'FavoritesCtrl'
      }
    }
  })
  .state('app.recently_watched', {
    url: '/recently_watched',
    views: {
      'menuContent': {
        templateUrl: 'templates/recently_watched.html',
        controller: 'RecentlyWatchedCtrl'
      }
    }
  })
  .state('app.map', {
    url: '/map',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('home');

});
