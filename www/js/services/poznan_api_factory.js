angular.module('app')
.factory('PoznanApi', function($http, $q, poznanApiStopInfo, poznanApiStops, $localStorage) {
  // 30 dni
  var expire = 1000 * 60 * 60 * 24 * 30;
  if (!$localStorage.stops) {
    $localStorage.stops = {data: null, expire: null};
  }

  function stopDirections(symbol) {
    return $q(function(resolve, reject) {
      $http({
        method: 'GET',
        url: poznanApiStopInfo,
        params: {stop_id: symbol},
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
      })
      .then(function(res) {
        if (res.data.stop) {
          resolve(parseRoutesData(res.data.routes));
        } else {
          reject('Error: There are no routes.');
        }
      }).
      catch(function(e) {
        reject(e);
      });
    });
  }

  function getStops() {
    return $q(function(resolve, reject) {
      if ($localStorage.stops.data && $localStorage.stops.expire > getCurrentTime()) {
        resolve($localStorage.stops.data);
      } else {
        $http({
          method: 'GET',
          url: poznanApiStops,
          headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        })
        .then(function(res) {
          $localStorage.stops.data = res.data.features;
          $localStorage.stops.expire = getCurrentTime() + expire;
          resolve(res.data.features);
        }).
        catch(function(e) {
          reject(e);
        });
      }
    });
  }

  function parseRoutesData(routes) {
    var directions = [];
    routes.forEach(function(route) {
      route.variants.forEach(function(variant) {
        directions.push({lineName: route.name, direction: variant.headsign});
      });
    });
    return directions;
  }

  function getCurrentTime() {
    return new Date().getTime();
  }

  return {
    stopDirections: stopDirections,
    getStops: getStops
  };
});
