angular.module('app')
.factory('VirtualMonitor', function($http, $httpParamSerializerJQLike, pekaApi, $ionicLoading, $q, Popups, $timeout) {

  function getTimes(symbol) {
    return apiQuery({method: 'getTimes', p0: '{"symbol":"' + symbol + '"}'});
  }

  function getStopPoints(pattern) {
    return apiQuery({method: 'getStopPoints', p0: '{"pattern":"' + pattern + '"}'});
  }

  function getBollardsByStopPoint(name) {
    return apiQuery({method: 'getBollardsByStopPoint', p0: '{"name":"' + name + '"}'});
  }

  function getStreets(pattern) {
    return apiQuery({method: 'getStreets', p0: '{"pattern":"' + pattern + '"}'});
  }

  function getBollardsByStreet(name) {
    return apiQuery({method: 'getBollardsByStreet', p0: '{"name":"' + name + '"}'});
  }

  function getLines(pattern) {
    return apiQuery({method: 'getLines', p0: '{"pattern":"' + pattern + '"}'});
  }

  function getBollardsByLine(name) {
    return apiQuery({method: 'getBollardsByLine', p0: '{"name":"' + name + '"}'});
  }

  function findMessagesForBollard(symbol) {
    return apiQuery({method: 'findMessagesForBollard', p0: '{"symbol":"' + symbol + '"}'});
  }


  function apiQuery(data) {
    $ionicLoading.show();
    return $q(function(resolve, reject) {
      $http({
        method: 'POST',
        url: pekaApi,
        data: $httpParamSerializerJQLike(data),
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        timeout: 3000
      })
      .then(function(res) {
        $ionicLoading.hide();
        resolve(res.data.success);
      })
      .catch(function(e) {
        $timeout(function() {
          Popups.connectionTimeout();
        }, 1000);
        reject(e);
      });
    });
  }

  return {
    getTimes: getTimes,
    getStopPoints: getStopPoints,
    getBollardsByStopPoint: getBollardsByStopPoint,
    getStreets: getStreets,
    getBollardsByStreet: getBollardsByStreet,
    getLines: getLines,
    getBollardsByLine: getBollardsByLine,
    findMessagesForBollard: findMessagesForBollard
  };
});
