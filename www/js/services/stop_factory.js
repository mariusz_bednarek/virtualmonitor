angular.module('app')
.factory('StopFactory', function($localStorage) {
  if (!$localStorage.favorites_stops) {
    $localStorage.favorites_stops = {tags: [], stops: []};
  }
  if (!$localStorage.recently_watched) {
    $localStorage.recently_watched = {tags: [], stops: []};
  }
  var favorites_stops = $localStorage.favorites_stops,
    recently_watched = $localStorage.recently_watched,
    index = 0;

  var getFavorites = function() {
    return favorites_stops.stops;
  };

  function isFavorite(tag) {
    if (favorites_stops.tags.indexOf(tag) != -1) {
      return true;
    } else {
      return false;
    }
  };

  function setFavorite(stop) {
    if (favorites_stops.tags.indexOf(stop.bollard.tag) == -1) {
      favorites_stops.tags.push(stop.bollard.tag);
      favorites_stops.stops.push(stop);
    }
  };

  function updateFavorite(stop) {
    index = favorites_stops.tags.indexOf(stop.bollard.tag);
    if ((index != -1) && (stop.directions.length > 0)) {
      favorites_stops.stops[index].directions = stop.directions;
    }
  }

  function removeFavorite(tag) {
    index = favorites_stops.tags.indexOf(tag);
    if (index != -1) {
      favorites_stops.tags.splice(index, 1);
      favorites_stops.stops.splice(index, 1);
    }
  };

  function getRecentrly() {
    return recently_watched.stops;
  };

  function addToRecently(stop) {
    index = recently_watched.tags.indexOf(stop.bollard.tag)
    if (index != -1) {
      // Update directions for stop
      recently_watched.stops[index].directions = stop.directions;
    } else {
      recently_watched.stops.unshift(stop);
      recently_watched.tags.unshift(stop.bollard.tag);
    }
  };

  return {
    getFavorites: getFavorites,
    isFavorite: isFavorite,
    setFavorite: setFavorite,
    removeFavorite: removeFavorite,
    updateFavorite: updateFavorite,
    getRecentrly: getRecentrly,
    addToRecently: addToRecently
  };
});
