angular.module('app')
.factory('MapApi', function(uiGmapGoogleMapApi, uiGmapIsReady, $q) {
  var map = null,
    marker = null,
    gMapsApi = null,
    mc = null
    infoWindow = null,
    markers = [];

  function init() {
    return $q(function(resolve, reject) {
      uiGmapGoogleMapApi.then(function(maps) {
        gMapsApi = maps;
        uiGmapIsReady.promise(1).then(function(instances) {
          instances.forEach(function(inst) {
            map = inst.map;
            mc = new MarkerClusterer(map, [], {imagePath: 'lib/markerclustererplus/images/m', gridSize: 50, maxZoom: 15});
            infoWindow = new gMapsApi.InfoWindow();
            resolve(gMapsApi);
          });
        });
      });
    });
  }

  function initMarker(data, addToMap) {
    if (addToMap) {
      marker = new gMapsApi.Marker(data);
      marker.setMap(map);
      markers.push(marker);
      return marker;
    } else {
      return new gMapsApi.Marker(data);
    }
  }

  function markerClusterer(markers) {
    mc.addMarkers(markers);
  }

  function showInfoWindow(_marker) {
    infoWindow.setContent(_marker.info);
    infoWindow.open(map, _marker);
  }

  function setPosition(position) {
    map.setCenter(new gMapsApi.LatLng(position.coords.latitude, position.coords.longitude));
    map.setZoom(16);
  }

  function clearMap() {
    if (mc) {
      mc.clearMarkers();
    }
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
  }

  return {
    init: init,
    initMarker: initMarker,
    markerClusterer: markerClusterer,
    showInfoWindow: showInfoWindow,
    setPosition: setPosition,
    clearMap: clearMap
  }
});
