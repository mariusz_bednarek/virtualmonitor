angular.module('app')
.factory('Popups', function($ionicPopup, $state, $window, $rootScope) {
  var noInternetConnectionPopup = null,
  connectionTimeoutPopup = null,
  disable = false;

  function noInternetConnection() {
    if (disable || (noInternetConnectionPopup !== null)) {
      return;
    }
    noInternetConnectionPopup = $ionicPopup.alert({
     title: 'Brak połaczenia z internetem',
     template: 'Aplikacja dla prawidłowego działania wymaga dostępu do internetu. Włącz wifi, lub dane pakietowe i kliknij "Ok" aby ponownie załadować aplikacje.'
   });

   noInternetConnectionPopup.then(function(res) {
     noInternetConnectionPopup = null;
    $window.location.reload(true);
   });
  }

  function connectionTimeout() {
    if (disable || (noInternetConnectionPopup !== null) || (connectionTimeoutPopup !== null)) {
      return;
    }
    connectionTimeoutPopup = $ionicPopup.alert({
     title: 'Błąd połączenia z serwerem PEKA',
     template: 'Wystąpił problem z połączeniem z serwerem PEKA. Kliknij "Ok" aby ponowić połączenie.'
   });

   connectionTimeoutPopup.then(function(res) {
      connectionTimeoutPopup = null;
      $state.reload();
   });
  }

  function getCurrentPositionError() {
    return $ionicPopup.confirm({
     title: 'Nie można pobrać pozycji',
     template: 'Włącz GPS w telefonie i naciśnij Odśwież.',
     okText: 'Odśwież',
     cancelText: 'Zamknij'
   });
  }

  function disablePopups() {
    disable = true;
  }

  function enablePopups() {
    disable = false;
  }

  return {
    noInternetConnection: noInternetConnection,
    connectionTimeout: connectionTimeout,
    disablePopups: disablePopups,
    enablePopups: enablePopups,
    getCurrentPositionError: getCurrentPositionError
  }
});
